#
# File: postgres-ref.py
# Auth: Martin Burolla
# Date: 7/27/2022
# Desc: Main entry point for the Python Postgres reference guide.
#

import data_access_pg


def main():
    # Get
    r1 = data_access_pg.get_books()
    print(r1)

    # Insert
    # r2 = data_access_pg.add_book("Marty was here", "123-232-1232-121")
    # print(r2)

    # Update
    # r3 = data_access_pg.update_book_title("Hi there", 53)
    # print(r3)


if __name__ == '__main__':   # dunder
    main()
