# Python Reference
Python code reference for beginners.

# Getting Started
- Clone this repo in new directory
- Open `python-reference` directory in IntelliJ
- Create virtual environment: `python -m venv venv`
    - You should see a new `venv` directory in the project
- Activate virtual environment: `venv\Scripts\activate.bat`
    - You should see `(env)` in the terminal window
- Install dependencies: `pip install -r requirements.txt`
- Optionally, add a Python configuration in IntelliJ to run the desired Python script

# Reference Guides
#### Basic Python
- [Functions 1](functions-1-ref.py)
- [Functions 2](functions-1-ref.py)
- [Truthy-Falsy](truthy-falsy-ref.py)
- [List Comprehensions](list-comprehenision-ref.py)
- [Lambda Filter Map](lambda-filter-map-ref.py)
- [Collections](collections-ref.py)

#### Data Access
- [Postgres Database](postgres-ref.py)
  - [data_access_pg](data_access_pg.py)   
- [S3](s3-ref.py)
  - [data_access_s3](data_access_s3.py)
- [SQS](sqs-ref.py)
  - [data_access_sqs](data_access_sqs.py)

# Python Data Types
- Text Type: `str`
- Numeric Types: `int`, `float`, `complex`
- Sequence Types:	`list`, `tuple`, `range`
- Mapping Type: `dict`
- Set Types: `set`, `frozenset`
- Boolean Type: `bool`
- Binary Types: `bytes`, `bytearray`, `memoryview`
- None Type: `NoneType`

# Call by Value vs Call by Reference
- Mutable types are passed by reference
- Immutable types are passed by value (AKA copy)
- Mutable types:
  - Lists
  - Dictionaries
  - Sets
- Immutable types:
  - Integers
  - Floats
  - Booleans
  - Strings
  - Tuples
  - Frozenset

# Notes
- Built with IntelliJ IDEA 2021.2.4 (Community Edition)
