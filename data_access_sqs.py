#
# File: data_access_sqs.py
# Auth: Martin Burolla
# Date: 7/26/2022
# Desc: Simple data access wrapper around AWS SQS
#
# VisibilityTimeout:
#  The length of time that a message received from a queue (by one consumer)
#  will not be visible to the other message consumers.
#
#  The message must be deleted before the Visibility Timeout has been reached
#  or else the message will be made available in the queue for other consumers
#  to read it.
#

import boto3
import logging

sqs = boto3.client('sqs')
QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/807758713182/siu-queue-1'


def send_message_to_sqs(user_name, user_id, body):
    response = sqs.send_message(
        QueueUrl=QUEUE_URL,
        DelaySeconds=10,
        MessageAttributes={
            'UserName': {
                'DataType': 'String',
                'StringValue': user_name
            },
            'UserId': {
                'DataType': 'Number',
                'StringValue': str(user_id)
            }
        },
        MessageBody=(
            body
        )
    )
    return response['MessageId']


def read_message_from_sqs():
    retval = None

    # Read message from SQS queue.
    response = sqs.receive_message(
        QueueUrl=QUEUE_URL,
        AttributeNames=['SentTimestamp'],
        MaxNumberOfMessages=1,
        MessageAttributeNames=['All'],
        VisibilityTimeout=30,
        WaitTimeSeconds=0
    )

    message = None
    try:
        message = response['Messages'][0]  # Only read one.
    except KeyError as ke:
        logging.info("SQS queue is empty.")

    if message:
        retval = {
            "message": message["Body"],
            "user_id": message["MessageAttributes"]["UserId"]["StringValue"],
            "user_name": message["MessageAttributes"]["UserName"]["StringValue"]
        }

        # Delete message once we have read it from the queue.
        receipt_handle = message['ReceiptHandle']
        sqs.delete_message(
            QueueUrl=QUEUE_URL,
            ReceiptHandle=receipt_handle
        )

    return retval
