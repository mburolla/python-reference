#
# File: sqs-ref.py
# Auth: Martin Burolla
# Date: 7/27/2022
# Desc: Main entry point for the Python SQS reference guide.
#

import data_access_sqs


def main():
    # SQS
    # Write
    r1 = data_access_sqs.send_message_to_sqs("Alice", 11111, "Liked a tweet.")
    r2 = data_access_sqs.send_message_to_sqs("Bob", 22222, "Posted a new tweet.")
    r3 = data_access_sqs.send_message_to_sqs("Charlie", 33333, "Unfollowed a user.")
    print(r1)
    print(r2)
    print(r3)

    # Read
    # message = data_access_sqs.read_message_from_sqs()
    # print(message)


if __name__ == '__main__':   # dunder
    main()
