#
# File: data_access_pg.py
# Auth: Martin Burolla
# Date: 7/26/2022
# Desc: Simple data access wrapper around Postgres DB
#

import os
import psycopg2
from psycopg2 import pool

SELECT_BOOKS = "select * from book"
INSERT_BOOK = "insert into book (title, isbn) values (%s, %s) returning book_id"
UPDATE_BOOK_TITLE = "update book set title = %s where book_id = %s returning book_id"

# The one and only Postgres connection pool.
pg_pool = psycopg2.pool.SimpleConnectionPool(1, 20,
                                             user="postgres",
                                             password="Ihgdp51505150!",
                                             host="localhost",
                                             database="express")


def get_books():
    # print(os.getenv('DB_PWD'))
    # print(os.getenv('DB_USER'))
    with pg_pool.getconn() as conn:
        with conn.cursor() as cur:
            cur.execute(SELECT_BOOKS)
            records = cur.fetchall()
    return records


def add_book(title, isbn):
    with pg_pool.getconn() as conn:
        with conn.cursor() as cur:
            cur.execute(INSERT_BOOK, (title, isbn))
            records = cur.fetchall()
            retval = records[0][0]
    return retval


def update_book_title(new_title, book_id):
    with pg_pool.getconn() as conn:
        with conn.cursor() as cur:
            cur.execute(UPDATE_BOOK_TITLE, (new_title, book_id))
            records = cur.fetchall()
            retval = records[0][0]
    return retval
